﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Graphics.Drawables;
using Android.Graphics;
using static Android.Views.View;
using System;
using System.Text;
using System.Threading;

namespace App7
{
    [Activity(Label = "App7", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {


       
        Connection con = new Connection();
        Thread t;
        Button pwcs;
        PopupWindow pw;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            t = new Thread(con.Receive);
            pw = new PopupWindow(this);


            
            base.OnCreate(savedInstanceState);
           


            
            SetContentView(Resource.Layout.Start);
            FindViewById<Button>(Resource.Id.buttonStart).Click += delegate {
                con.Connect("192.168.97.82");
                mainWindow(); };



            Button button1 = FindViewById<Button>(Resource.Id.button1);


        }
        public void mainWindow()
        {
            SetContentView(Resource.Layout.Main);
            pwcs = FindViewById<Button>(Resource.Id.button1);
            pw.DismissEvent += SendChange;


            for (int i = 0; i < 64; i++)
            {
                Button b = FindViewById<Button>(2131099655 + i);

                b.Text = "";
                b.SetBackgroundColor(Color.Gray);

                b.Click += delegate
                {

                    pwcs = b;
                    View view = LayoutInflater.Inflate(Resource.Layout.layout1, null);
                    pw.ContentView = view;
                    (view.FindViewById<Button>(Resource.Id.buttonr)).Click += delegate { pwcs.SetBackgroundColor(Color.Red); pw.Dismiss(); };
                    (view.FindViewById<Button>(Resource.Id.buttonb)).Click += delegate { pwcs.SetBackgroundColor(Color.Blue); pw.Dismiss(); };
                    (view.FindViewById<Button>(Resource.Id.buttong)).Click += delegate { pwcs.SetBackgroundColor(Color.Green); pw.Dismiss(); };
                    (view.FindViewById<Button>(Resource.Id.buttonw)).Click += delegate { pwcs.SetBackgroundColor(Color.White); pw.Dismiss(); };
                    pw.SetBackgroundDrawable(new BitmapDrawable());
                    pw.OutsideTouchable = true;
                    //pw.ShowAsDropDown(button);
                    
                    pw.ShowAtLocation(b, GravityFlags.NoGravity, (int)b.GetX() + 50, (int)(((LinearLayout)b.Parent).GetY() + 300));
                    


                };


            }

            FindViewById<Button>(Resource.Id.button65).Click += SendCheck;
            FindViewById<Button>(Resource.Id.button66).Click += Abort;

        }

        private void SendChange(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            TextView tv = FindViewById<TextView>(Resource.Id.textView1);
            int id = pwcs.Id - 2131099655;
            tv.Text = id.ToString();
            sb.Append(id.ToString());
            if(((ColorDrawable)pwcs.Background).Color == Color.Red)
            {
                tv.Text += " Red";
                sb.Append("r");
            }  else if (((ColorDrawable)pwcs.Background).Color == Color.Blue) {
                tv.Text += " Blue";
                sb.Append("b");
            } else if (((ColorDrawable)pwcs.Background).Color == Color.Green)
            {
                tv.Text += " Green";
                sb.Append("g");
            } else if (((ColorDrawable)pwcs.Background).Color == Color.White)
            {
                tv.Text += " White";
                sb.Append("w");
            }
            else if (((ColorDrawable)pwcs.Background).Color == Color.Gray)
            {
                tv.Text += " Gray";
                sb.Append("0");
            }
            //c.Send(sb.ToString());
        }


        public void SendCheck (object sender, EventArgs e)
        {
            TextView tv = FindViewById<TextView>(Resource.Id.textView1);
            Button b; StringBuilder sb = new StringBuilder(); 
            for (int i = 0; i < 64; i++)
            {
                b = FindViewById<Button>(2131099655 + i);
                if (((ColorDrawable)b.Background).Color == Color.Red)
                {
                    sb.Append("r");
                }
                else if (((ColorDrawable)b.Background).Color == Color.Blue)
                {
                    sb.Append("b");
                }
                else if (((ColorDrawable)b.Background).Color == Color.Green)
                {
                    sb.Append("g");
                }
                else if (((ColorDrawable)b.Background).Color == Color.White)
                {
                    sb.Append("w");
                } else
                {
                    sb.Append("0");
                }
                
            }
            tv.Text = sb.ToString();
            con.Send(sb.ToString());
        }
        public void Abort(object sender, EventArgs e)
        {
            //c.Send("abort");
            TextView tv = FindViewById<TextView>(Resource.Id.textView1);
        }
    }
}


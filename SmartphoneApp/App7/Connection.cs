﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net.Sockets;

namespace App7
{
    public class RecEventArgs
    {
        public RecEventArgs(string s) { Text = s; }
        public String Text { get; private set; }
    }
    class Connection
    {
        TcpClient tc;
        NetworkStream ns;
        public string data;
        public string Data
        {

            get
            {
                return data;
            }
            set
            {
                data = value; OnRecChanged();
            }
        }
        byte[] rec;
        public delegate void RecEventHandler(object sender, RecEventArgs e);
        public event RecEventHandler RecChanged;
        protected virtual void OnRecChanged()
        {

            if (RecChanged != null)
                RecChanged(this, new RecEventArgs("Hello"));
        }
        public void Connect(string s)
        {
            tc = new TcpClient(s, 50000);
            //tc = new TcpClient("192.168.43.131", 50000);
            //tc.Connect(IPAddress.Parse("192.168.1.6"), 50000);
            ns = tc.GetStream();
        }
        public void Send(string s)
        {
            ns = tc.GetStream();
            ns.Write(Encoding.ASCII.GetBytes(s), 0, Encoding.ASCII.GetBytes(s).Length);


        }
        public void Receive()
        {

            while (true)
            {
                rec = new byte[tc.ReceiveBufferSize];
                ns.Read(rec, 0, tc.ReceiveBufferSize);
                Data = Encoding.ASCII.GetString(rec);

                if (data.Contains("stop"))
                {
                    break;
                }

                //return  Encoding.ASCII.GetString(rec); 
            }
            ns.Close();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ComputerApp
{
    public class RecEventArgs
    {
        public RecEventArgs(string s) { Text = s; }
        public String Text { get; private set; }
    }
    class Connection
    {
        TcpListener tl = new TcpListener(IPAddress.Any,50000);
        TcpClient tc;
        NetworkStream ns;
        public string data;
        public string Data {

            get
            {
                return data;
            }
            set
            {
                data = value; OnRecChanged();
            }
        }
        byte[] rec; // Buffer zum Daten lesen
        public delegate void RecEventHandler(object sender, RecEventArgs e);
        public event RecEventHandler RecChanged;
        protected virtual void OnRecChanged()
        {

            if (RecChanged != null)
                RecChanged(this, new RecEventArgs("Hello"));
        }
        
        public void StartListen()
        {
            tl.Start(); // TCP Listener wird gestartet
            tc = tl.AcceptTcpClient(); // TCP Client wird akzeptiert
            ns = tc.GetStream(); // Client Stream  wird auf ns NetworkStream gesetzt
        }

        public void Send(string s)
        {
            ns = tc.GetStream(); 

            ns.Write(Encoding.ASCII.GetBytes(s), 0, Encoding.ASCII.GetBytes(s).Length);
        }

        public void Receive()
        {
            while (true)
            {
                rec = new byte[tc.ReceiveBufferSize];
                ns.Read(rec, 0, tc.ReceiveBufferSize);
                Data = Encoding.ASCII.GetString(rec);
                
                if(data.Contains("stop") )
                {
                    break;
                }

                //return  Encoding.ASCII.GetString(rec); 
            }
            ns.Close();
            
        } 
    }
}

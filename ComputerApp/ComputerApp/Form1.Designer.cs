﻿namespace ComputerApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.serialPortMain = new System.IO.Ports.SerialPort(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.cboComPort = new System.Windows.Forms.ComboBox();
            this.cmdOpenPort = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lblPortInfo = new System.Windows.Forms.Label();
            this.cmdClosePort = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 47);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "COM-Port:";
            // 
            // cboComPort
            // 
            this.cboComPort.FormattingEnabled = true;
            this.cboComPort.Location = new System.Drawing.Point(103, 45);
            this.cboComPort.Margin = new System.Windows.Forms.Padding(2);
            this.cboComPort.Name = "cboComPort";
            this.cboComPort.Size = new System.Drawing.Size(179, 21);
            this.cboComPort.TabIndex = 1;
            // 
            // cmdOpenPort
            // 
            this.cmdOpenPort.Location = new System.Drawing.Point(103, 101);
            this.cmdOpenPort.Margin = new System.Windows.Forms.Padding(2);
            this.cmdOpenPort.Name = "cmdOpenPort";
            this.cmdOpenPort.Size = new System.Drawing.Size(68, 28);
            this.cmdOpenPort.TabIndex = 2;
            this.cmdOpenPort.Text = "Port öffnen";
            this.cmdOpenPort.UseVisualStyleBackColor = true;
            this.cmdOpenPort.Click += new System.EventHandler(this.cmdOpenPort_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 171);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Info:";
            // 
            // lblPortInfo
            // 
            this.lblPortInfo.AutoSize = true;
            this.lblPortInfo.Location = new System.Drawing.Point(103, 171);
            this.lblPortInfo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPortInfo.Name = "lblPortInfo";
            this.lblPortInfo.Size = new System.Drawing.Size(16, 13);
            this.lblPortInfo.TabIndex = 4;
            this.lblPortInfo.Text = "...";
            // 
            // cmdClosePort
            // 
            this.cmdClosePort.Location = new System.Drawing.Point(198, 101);
            this.cmdClosePort.Margin = new System.Windows.Forms.Padding(2);
            this.cmdClosePort.Name = "cmdClosePort";
            this.cmdClosePort.Size = new System.Drawing.Size(82, 28);
            this.cmdClosePort.TabIndex = 5;
            this.cmdClosePort.Text = "Port schließen";
            this.cmdClosePort.UseVisualStyleBackColor = true;
            this.cmdClosePort.Click += new System.EventHandler(this.cmdClosePort_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(126, 247);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "label3";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(427, 342);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmdClosePort);
            this.Controls.Add(this.lblPortInfo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmdOpenPort);
            this.Controls.Add(this.cboComPort);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.IO.Ports.SerialPort serialPortMain;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboComPort;
        private System.Windows.Forms.Button cmdOpenPort;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblPortInfo;
        private System.Windows.Forms.Button cmdClosePort;
        private System.Windows.Forms.Label label3;
    }
}


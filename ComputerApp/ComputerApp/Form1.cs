﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;

namespace ComputerApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Connection c;
        String s;
        Thread t;
        string pattern;
        private void Form1_Load(object sender, EventArgs e)
        {
            c = new Connection();
            c.StartListen();
            c.RecChanged += delegate { this.label3.BeginInvoke((MethodInvoker)delegate () { this.label3.Text = this.c.data.ToString(); ; }); };
            t = new Thread(c.Receive);
            t.Start();
            //t Thread gets "game start" message from decive
            StartGame();

            cmdClosePort.Enabled = false;
            // Lädt alle offenen COM Ports in ein String Array
            // Ports werden wie folgt ausgegeben "COM1", "COM2", ...
            string[] ports = SerialPort.GetPortNames();
            // für jedes Element des String Arrays (für jede COM Port)
            // wird ein Element hinzugefügt
            cboComPort.Items.AddRange(ports);
            // Hier wird der erste COM Port automatisch ausgewählt
            if (cboComPort.Items.Count > 0)
            {
                cboComPort.SelectedIndex = 0;
            }
        }


        #region OpenPort
        private void cmdOpenPort_Click(object sender, EventArgs e)
        {
            // Wenn in der ComboBox kein Port ausgewählt
            // wurde wird -1 zurückgegeben
            // Überprüfen ob COM Port ausgewählt wurde
            if (cboComPort.SelectedIndex == -1)
            {
                MessageBox.Show("Bitte COM-Port auswählen!");
            }
            else
            // wenn ein Port ausgewählt wurde...
            {
                // der in die WinForm gezogene SerialPortMain wird
                // auf den gewählten Port gesetzt
                serialPortMain.PortName = cboComPort.SelectedItem.ToString();
                // sollte der ComPort offen sein, verbinden wir uns mit diesem
                // und schreiben in das Info Label, dass Port geöffnet wurde
                if (!serialPortMain.IsOpen)
                {
                    // der Port wird geöffnet
                    serialPortMain.Open();
                    lblPortInfo.Text = "COM-Port wurde geöffnet";
                    // "Port öffnen" Button wird ausgeschaltet
                    cmdOpenPort.Enabled = false;
                    // Port schließen" Button wird eingeschaltet
                    cmdClosePort.Enabled = true;
                    StartRound();
                }
                else
                {
                    // sollte der COM Port schon offen sein...
                    lblPortInfo.Text = "Öffne des COM-Ports fehlgeschlagen";
                }
            }
        }
        #endregion

        #region ClosePort
        // Button ClosePort event
        private void cmdClosePort_Click(object sender, EventArgs e)
        {
            // Port kann nicht geschlossen werden wenn er
            // nicht offen ist
            // Prüfen ob Port offen ist
            if (serialPortMain.IsOpen)
            {
                // Port wird geschlossen
                serialPortMain.Close();
                lblPortInfo.Text = "COM-Port wurde geschlossen";
                // "Port schließen" Button wird ausgeschaltet 
                cmdClosePort.Enabled = false;
                // // "Port öffnen" Button wird eingeschaltet
                cmdClosePort.Enabled = true;
            }
            else
            {
                lblPortInfo.Text = "COM-Port bereits geschlossen";
            }
        }
        #endregion

        #region GameHandler
        // Connection instanzieren...

        // PatternGenerator instanzieren...
        PatternGenerator pgen = new PatternGenerator();

        private void StartGame()
        {
            int HighScore = 0;
        }

        private void StartRound()
        {
            string um_map = "";
            // Erhält laufend neuen Input
            string recievedInput ="";
            // Generiert Matrix Muster
            string bmap = pgen.GeneratePattern();
            pattern = bmap;
            
            // Schreibe Muster an serialPort aus
            serialPortMain.Write(bmap + '\n');
            // Hällt Programm für 5 Sekunden
            //Thread.Sleep(5000);
            // Teile Device mit das es Input aufnimmt
            //c.Send("ready");

            // Solange der Benutzer nicht auf den "Bestätigen" Button drückt...
            // ...wird jede Veränderung gesendet und am uC angezeigt.
            // Wird der bestätigen Button gedrückt wird ein String mit der...
            // ...vom Benutzer erstellten Map gesendet und überprüft.
            //while (recievedInput != "check")
            //{
                c.RecChanged += delegate { if (c.data.Length > 3) { PatternCheck(c.data); } else { SendChange(); } };
                // recievedInput = con.Receive();
            //    serialPortMain.WriteLine(recievedInput);
            //}
            // um_map = con.Receive();
            // Wenn von Benutzer erstelltes Muster korrekt ist wird 1 zurückgegeben...
            // wenn es falsch ist 0.
            //if (um_map == bmap)
            //{
            //    // Sende Feedback an Device
            //    c.Send("1");
            //    // Sende Feedback an uC...
            //    serialPortMain.WriteLine("true");
            //}
            //else
            //{
            //    // Sende Feedback an Device
            //    c.Send("0");
            //    // Sende Feedback an uC...
            //    serialPortMain.WriteLine("false");
            //}
            //Thread.Sleep(2000);
        }
        #endregion
        public void PatternCheck(string muster) {
            if (muster == pattern) {
                c.Send("true");
                serialPortMain.Write("checkmark");
            } else {
                c.Send("false");
                serialPortMain.Write("cross");
            }  }
        public void SendChange() { serialPortMain.WriteLine(c.data + '\n'); }
    }
}

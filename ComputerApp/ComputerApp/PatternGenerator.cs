﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComputerApp
{
    class PatternGenerator
    {
        // Erstellung eines Random Objekts
        Random Muster = new Random();
        // Wie viele LEDs leuchten werden
        private int amountofLEds;
        // Der Index der LEDs die leuchten sollen
        private int[] selectedLEDs;
        // Wert von 1-5 der bestimmt in welcher Farbe
        // die LED leuchten sollte
        private int colour;
        // 64int großes Array dass das Muster abspeichert
        // bestimmt ob die LEDs ein oder aus bzw. in
        // welcher Farbe sie leuchten
        private int[] Bitmap = new int[64];

        #region Pattern Generator
        // generiert ein zufälliges Muster
        public string GeneratePattern()
        {
            // Verschieden Indexer
            int i1 = 0;
            int i2 = 0;
            int i3 = 0;
            int BitMapLEDIndex = 0;

            // amountofLEDs erhält einen Wert zwischen
            // 5 und 15
            amountofLEds = RandomLEDAmount();
            // selectedLEDs wird auf Wert angepasst
            selectedLEDs = new int[amountofLEds];

            // solange der Index kleiner wie amountofLEDs
            // ist wird zufällig eine LED ausgewält
            while (i1 < amountofLEds)
            {
                selectedLEDs[i1] = RandomLEDSelector();
                i1++;
            }

            // solange der Index kleiner gleich 63 ist
            while (i2 <= 63)
            {
                // eine Bitmap das durch einen Index von 0 bis 63
                // besteht wird erstellt
                Bitmap[i2] = BitMapLEDIndex;
                BitMapLEDIndex = BitMapLEDIndex + 1;
                i2++;
            }

            // solange der Index kleiner gleich 63 ist
            while (i3 <= 63)
            {
                // hier wird der Index durch einen Wert getauscht
                if (selectedLEDs.Contains(Bitmap[i3]))
                {
                    // wenn der ausgwählte Index (Bitmap[x]) in
                    // dem selectedLEDs Array vorhanden ist
                    // wird im eine zufällige Farbe gegeben
                    Bitmap[i3] = RandomColourSelector();
                }
                else
                {
                    // wenn das nicht der Fall ist wird ein
                    // 0 eingesetzt, die LED leuchtet nicht
                    Bitmap[i3] = 0;
                }
                i3++;
            }
            // Das zufällig erstellte Muster wird zurückgegeben
            // Zu string Konvertieren
            string sBitmap = string.Join("", Bitmap);
            return sBitmap;
        }
        #endregion

        #region RandomSelector
        // Wählt eine zufällige Anzahl (zwischen 5-15)
        // an LEDs aus die Leuchten sollen
        public int RandomLEDAmount()
        {
            int amount = Muster.Next(5, 15);
            return amount;
        }

        // Wählt zufällig eine Zahl zwischen 0 und 64
        // und gibt diese zurück
        public int RandomLEDSelector()
        {
            int selected = Muster.Next(0, 63);
            return selected;
        }

        // Wählt zufällig eine Zahl zwischen 0 und 5 die
        // den Index einer Farbe wiederspiegelt
        public int RandomColourSelector()
        {
            int colour = Muster.Next(1, 5);
            return colour;
        }
        #endregion RandomSelector
    }
}
